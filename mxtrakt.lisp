(in-package :mxtrakt)

(defparameter *mxtrakt-duration-ratios* '(8 7 6 5 4 3 2 1 3/4 2/3 1/2 1/4 1/3 1/6 1/12 1/24 1/8 1/32 1/64 0))

(defun onsets (file &key (method "default")
		      (window-size 1024)
		      (hop-size 256)
		      (count-end t))
  "extract list of onset times from an audio file"
  (let* ((samplerate 0)	 
	 (read_l 0)
	 (read (cffi:foreign-alloc :int :initial-element read_l))	 
	 (n_frames 0)
	 (source (new_aubio_source file samplerate hop-size))
	 onset
	 onset-list)
    (cffi::with-foreign-objects ((in '(:struct cl-aubio::fvec_t))
				 (out '(:struct cl-aubio::fvec_t)))
      (setf in (new_fvec hop-size))
      (setf out (new_fvec 2))
      ;; not sure why this isn't done in the method above ...
      ;; but it's the same in the C example that served as
      ;; a template for this ... 
      (setf samplerate (aubio_source_get_samplerate source))
      ;; samplerate detection seems to be not so relieable, so set
      ;; the onset object can only be created now ... 
      (setf onset (new_aubio_onset method window-size hop-size samplerate))      
      (setf onset-list
	    (loop named detect
	       while (progn (aubio_source_do source in read) ;; read from source
			    (setf n_frames (+ n_frames (mem-aref read ':int)))
			    (eql (mem-aref read ':int) hop-size))
	       for os = (progn 		   
			 (aubio_onset_do onset in out)	    
			 (unless (eql 0.0 (mem-aref (cffi::foreign-slot-value
						 out '(:struct cl-aubio::fvec_t)
						 'cl-aubio::data)
						    ':float 0))			   
			   (aubio_onset_get_last_ms onset)))
	       when os collect os))
      (when count-end
	(nconc onset-list (list (* 1000 (float (/ n_frames samplerate))))))
      ;; cleanup 
      (del_aubio_source source)
      (del_aubio_onset onset)
      (del_fvec in)
      (del_fvec out)
      (aubio_cleanup))
    onset-list))

(defun pitches (file &key (method "default")
		      (window-size 1024)
		      (hop-size 256))
  "extract list of pitches (block-wise) from an audio file"
  (let* ((samplerate 0)	 
	 (read_l 0)
	 (read (cffi:foreign-alloc :int :initial-element read_l))	 
	 (source (new_aubio_source file samplerate hop-size))
	 pitch
	 pitch-list)
    (cffi::with-foreign-objects ((in '(:struct cl-aubio::fvec_t))
				 (out '(:struct cl-aubio::fvec_t)))
      (setf in (new_fvec hop-size))
      (setf out (new_fvec 1))
      ;; not sure why this isn't done in the method above ...
      ;; but it's the same in the C example that served as
      ;; a template for this ... 
      (setf samplerate (aubio_source_get_samplerate source))
      ;; samplerate detection seems to be not so relieable, so set
      ;; the pitch object can only be created now ... 
      (setf pitch (new_aubio_pitch method window-size hop-size samplerate))      
      (setf pitch-list
	    (loop named detect
	       while (progn (aubio_source_do source in read) ;; read from source
			    (eql (mem-aref read ':int) 256))
	       for os = (progn 		   
			 (aubio_pitch_do pitch in out)	    
			 (unless (eql 0.0 (mem-aref (cffi::foreign-slot-value
						 out '(:struct cl-aubio::fvec_t)
						 'cl-aubio::data)
						':float 0))	        
			   (fvec_get_sample out 0)))
	       when os collect os))
      ;; cleanup 
      (del_aubio_source source)
      (del_aubio_pitch pitch)
      (del_fvec in)
      (del_fvec out)
      (aubio_cleanup))
    pitch-list))

(defun inter-onset-durations (file &key (method "default")
			 (window-size 1024)
			 (hop-size 256))
  "extract list of durations between onsets (not counting silence) from an audio file"
  (loop for (a b) on (onsets file
			     :method method
			     :window-size window-size
			     :hop-size hop-size)
     while b
     collect (- b a)))

(defun find-closest-in-sorted-list (value numbers)
  "find the numerically closest value in an ascending list of numbers"
  (if (eql (length numbers) 1)
      (car numbers)
      (let* ((middle (floor (/ (length numbers) 2)))
	     (upper (subseq numbers middle))
	     (lower (subseq numbers 0 middle))
	     (middle-val (nth middle numbers))
	     (diff-low (abs (- value (car (last lower)))))
	     (diff-hi (abs (- value (car upper)))))
	(cond ((eql value middle-val) value)
	      ((> diff-low diff-hi) (find-closest-in-sorted-list value upper))
	      ((< diff-low diff-hi) (find-closest-in-sorted-list value lower))))))

(defun quantize (central durations &key (ratios *mxtrakt-duration-ratios*))
  "quantize durations to a certain central duration (pretty heuristic)"
  (let* ((central-cooked (cond ((typep central 'number) central)
			      ((eq central 'mean) (alexandria:mean durations))
			      ((eq central 'median) (alexandria:median durations))
			      (t central)))
	(possible-durations (sort (mapcar #'(lambda (ratio) (float (* central-cooked ratio))) ratios) #'>)))
    (mapcar #'(lambda (dur) (find-closest-in-sorted-list dur possible-durations)) durations)))

