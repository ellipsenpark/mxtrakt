;;;; mxtrakt provides convenience functions around cl-aubio.
;;;; Copyright 2018 Niklas Reppel
;;;;
;;;; mxtrakt licensed under GPLv3 or later
(asdf:defsystem #:mxtrakt
  :serial t
  :author "Niklas Reppel"
  :description "mxtrakt provides convenience functions around cl-aubio"
  :license "GPLv3"
  :depends-on (#:cffi #:cl-aubio #:alexandria)
  :components ((:file "package")
               (:file "mxtrakt")))
