;;;; mxtrakt provides convenience functions around cl-aubio.
;;;; Copyright 2018 Niklas Reppel
;;;;
;;;; mxtrakt licensed under GPLv3 or later
(defpackage #:mxtrakt
  (:use :cl :cffi :cl-aubio)
  (:export
   :onsets
   :inter-onset-durations
   :pitches
   :quantize))
